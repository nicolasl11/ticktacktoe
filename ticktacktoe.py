board = []

currentPlayer = None

tile = {
	"EMPTY": " ",
	"X": "x",
	"O": "o"
}

PLAYER_1 = {
	"text": "player 1 (x)",
	"id": 1,
	"t": tile["X"]
}

PLAYER_2 = {
	"text": "player 2 (o)",
	"id": 2,
	"t": tile["O"]
}

def start():
	global currentPlayer
	currentPlayer = PLAYER_1
	for i in range(3 * 3):
		board.append(tile["EMPTY"])


def stop():
	global currentPlayer
	print("the winner is " + currentPlayer["text"])


def draw():
	print("+---+---+---+")
	for i in range(0, 3 * 3, 3):
		print("| " + board[i] + " | " + board[i + 1] + " | " + board[i + 2] + " |")
		print("+---+---+---+")

	
def check():
	checks = [
		[1, 2, 3],
		[4, 5, 6],
		[7, 8, 9],

		[1, 4, 7],
		[2, 5, 8],
		[3, 6, 9],

		[1, 5, 9],
		[3, 5, 7]
	]

	global currentPlayer

	for ch in checks:
		if board[ch[0]] == currentPlayer["t"] and board[ch[1]] == currentPlayer["t"] and board[ch[2]] == currentPlayer["t"]:
			return True

	return False


def update():
	global currentPlayer

	i = input("choos your spot " + currentPlayer["text"] + " (hor,ver): ")

	x = int(i[0]) - 1
	y = int(i[2]) - 1

	if board[x + y * 3] == tile["EMPTY"]:
		board[x + y * 3] = currentPlayer["t"]

	if check():
		return False

	if currentPlayer["id"] == PLAYER_1["id"]:
		currentPlayer = PLAYER_2
	else:
		currentPlayer = PLAYER_1

	return True


start()

running = True

while running:
	draw()
	running = update()

draw()
stop()
