#!/bin/bash

BOARD=()

TILE_EMPTY=' '
TILE_X='x'
TILE_O='o'

PLAYER_1=('player 1 (x)' 1 $TILE_X)
PLAYER_2=('player 2 (o)' 2 $TILE_O)

CURRENT_PLAYER=

function startGame {
	CURRENT_PLAYER=("${PLAYER_1[@]}")
	for i in $(seq 0 8); do
		BOARD[$i]=$TILE_EMPTY
	done
}

function printBoard {
	echo "+---+---+---+"
	for i in $(seq 0 3 8); do
		echo "| ${BOARD[$i]} | ${BOARD[($i + 1)]} | ${BOARD[($i + 2)]} |"
		echo "+---+---+---+"
	done
}

function stopGame {
	echo "the winner is ${CURRENT_PLAYER[0]}"
}

function update {
	echo -n "choose your spot ${CURRENT_PLAYER[0]}: "
	read INPUT

	if [ "$INPUT" = "" ]; then
		echo "invalide input"
		return 1
	fi

	X=$(( $(cut -d , -f 1 <(echo "$INPUT")) - 1 ))
	Y=$(( $(cut -d , -f 2 <(echo "$INPUT")) - 1 ))

	if [ "$X" = "" -o "$Y" = "" ]; then
		echo "invalide input detected"
		return 1
	fi

	COOR=$(($X + $Y * 3))
	echo "$COOR"

	if [ ${BOARD[$COOR]} = $TILE_EMPTY ]; then
		BOARD[$COOR]=${CURRENT_PLAYER[2]}
	fi

	check
	if [ "$?" -eq 1 ]; then
		return 0
	fi

	if [ "${CURRENT_PLAYER[1]}" -eq 1 ]; then
		CURRENT_PLAYER=("${PLAYER_2[@]}")
	else
		CURRENT_PLAYER=("${PLAYER_1[@]}")
	fi

	return 1
}

function check {
	CHECKS=(0 1 2 3 4 5 6 7 8 0 3 6 1 4 7 2 5 8 0 4 8 2 4 6)

	for i in $(seq 0 3 9); do
		echo -e "\$i = $i"
		i1=$(($i + 1))
		i2=$(($i + 2))

		if [ "${BOARD[${CHECKS[$i]}]}" = ${CURRENT_PLAYER[2]} -a \
		   	"${BOARD[${CHECKS[$i1]}]}" = ${CURRENT_PLAYER[2]} -a \
			"${BOARD[${CHECKS[$i2]}]}" = ${CURRENT_PLAYER[2]} ]; then
			return 1
		fi
	done

	return 0
}

RUNNING=1

startGame

while [ "$RUNNING" -eq 1 ]; do
	printBoard
	update
	RUNNING=$?
done

printBoard
stopGame
