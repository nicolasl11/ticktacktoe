package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

const TILE_X string = "x"
const TILE_O string = "o"
const TILE_EMPTY string = " "

var p1, p2, currentPlayer Player
var board []string

type Player struct {
	Text string
	Id   int
	T    string
}

func main() {
	var running bool = true

	p1 = Player{
		Text: "player 1 (x)",
		Id:   1,
		T:    TILE_X,
	}

	p2 = Player{
		Text: "player 2 (o)",
		Id:   2,
		T:    TILE_O,
	}

	start()

	for running {
		draw()
		running = update()
	}

	draw()
	stop()
}

func start() {
	currentPlayer = p1
	for i := 0; i < 9; i++ {
		board = append(board, TILE_EMPTY)
	}
}

func stop() {
	fmt.Printf("the winner is %s\n", currentPlayer.Text)
}

func draw() {
	fmt.Printf("+---+---+---+\n")
	for i := 0; i < 9; i += 3 {
		fmt.Printf("| %s | %s | %s |\n", board[i], board[i+1], board[i+2])
		fmt.Printf("+---+---+---+\n")
	}
}

func update() bool {
	reader := bufio.NewReader(os.Stdin)
	fmt.Printf("choose your spot %s: ", currentPlayer.Text)
	input, _ := reader.ReadString('\n')
	input = strings.Split(input, "\n")[0]
	coord := strings.Split(input, ",")

	x, err := strconv.Atoi(coord[0])
	y, err := strconv.Atoi(coord[1])
	fmt.Println(coord)

	x -= 1
	y -= 1

	if err != nil {
		fmt.Printf("invalid input\n")
		return true
	}

	if board[x+y*3] == TILE_EMPTY {
		board[x+y*3] = currentPlayer.T
	}

	if check() {
		return false
	}

	if currentPlayer.Id == p1.Id {
		currentPlayer = p2
	} else {
		currentPlayer = p1
	}

	return true
}

func check() bool {
	checks := [][]int{
		{0, 1, 2},
		{3, 4, 5},
		{6, 7, 8},

		{0, 3, 6},
		{1, 4, 7},
		{2, 5, 8},

		{0, 4, 8},
		{2, 4, 6},
	}

	for i := 0; i < 8; i++ {
		if board[checks[i][0]] == currentPlayer.T && board[checks[i][1]] == currentPlayer.T && board[checks[i][2]] == currentPlayer.T {
			return true
		}
	}

	return false
}
