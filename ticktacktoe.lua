local board = {}

local currentPlayer = nil

local tile = {
	EMPTY = " ",
	X = "x",
	O = "o"
}

local PLAYER_1 = {
	text = "player 1 (x)",
	id = 1,
	t = tile.X
}

local PLAYER_2 = {
	text = "player 2 (o)",
	id = 2,
	t = tile.O
}

function start()
	currentPlayer = PLAYER_1
	for i = 1, 3 * 3 do
		table.insert(board, tile.EMPTY)
	end
end

function stop()
	print("the winner is " .. currentPlayer.text)
end

function update()
	io.write("choose your spot " .. currentPlayer.text .. " (hor,vert): ")
	local input = io.read()
	local x = input:sub(1, 1)
	local y = input:sub(3, 3)

	if board[x + (y * 3 - 3)] == tile.EMPTY then
		board[x + (y * 3 - 3)] = currentPlayer.t
	end

	if check() then
		return false
	end

	if currentPlayer == PLAYER_1 then
		currentPlayer = PLAYER_2
	else
		currentPlayer = PLAYER_1
	end

	return true
end

function draw()
	print("+---+---+---+")
	for i = 1, 3 * 3, 3 do
		print("| " .. board[i] .. " | " .. board[i + 1] .. " | " .. board[i + 2] .. " |")
		print("+---+---+---+")
	end
end

function check()
	local checks = {
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},

		{1, 4, 7},
		{2, 5, 8},
		{3, 6, 9},
		
		{1, 5, 9},
		{3, 5, 7}
	}

	for key, value in pairs(checks) do
		if board[value[1]] == currentPlayer.t and
			board[value[2]] == currentPlayer.t and
			board[value[3]] == currentPlayer.t then
			return true
		end
	end

	return false
end

start()

local running = true

while running do
	draw()
	running = update()
end

draw()
stop()
