object ticktacktoe {
	val TILE_X = "x"
	val TILE_O = "o"
	val TILE_EMPTY = " "

	val p1 = new Player("player 1 (x)", 1, TILE_X)
	val p2 = new Player("palyer 2 (o)", 2, TILE_O)

	var currentPlayer: Player = null
	var board = new Array[String](9)

	def start(): Unit = {
		currentPlayer = p1
		for (i <- 0 to (board.length - 1)) {
			board(i) = TILE_EMPTY
		}
	}

	def stop(): Unit = {
		println("the winner is " + currentPlayer.text)
	}

	def draw(): Unit = {
		println("+---+---+---+")
		for (i <- 0 until 9
			 if i % 3 == 0) {
			println(s"| ${board(i)} | ${board(i + 1)} | ${board(i + 2)} |")
			println("+---+---+---+")
		}
	}

	def update(): Boolean = {
		val input = Console.readLine

		val x: Int = input(0) - 49
		val y: Int = input(2) - 49

		if (board(x + y * 3) == TILE_EMPTY) {
			board(x + y * 3) = currentPlayer.t
		}

		if (check()) {
			return false
		}

		if (currentPlayer.id == 1) {
			currentPlayer = p2
		} else {
			currentPlayer = p1
		}

		true
	}

	def check(): Boolean = {
		val checks = Array(
			Array(0, 1, 2),
			Array(3, 4, 5),
			Array(6, 7, 8),
			Array(0, 3, 6),
			Array(1, 4, 7),
			Array(2, 5, 8),
			Array(0, 4, 7),
			Array(2, 4, 6)
			)

		for (c <- 0 until 8) {
			if (board(checks(c)(0)) == currentPlayer.t &&
				board(checks(c)(1)) == currentPlayer.t &&
				board(checks(c)(2)) == currentPlayer.t) {
				return true
			}
		}

		false
	}

	def main(args: Array[String]): Unit = {
		start()

		var running = true

		while (running) {
			draw()
			running = update()
		}

		draw()
		stop()
	}

	class Player(val text: String, val id: Int, val t: String)
}
