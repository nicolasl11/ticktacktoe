import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class ticktacktoe {
	public static final String TILE_X = "x";
	public static final String TILE_O = "o";
	public static final String TILE_EMPTY = " ";

	public static List<String> board;
	public static Player currentPlayer, p1, p2;

	public static void main(String[] args) {
		p1 = new Player();
		p1.text = "player 1 (x)";
		p1.id = 1;
		p1.t = TILE_X;

		p2 = new ticktacktoe.Player();
		p2.text = "player 2 (o)";
		p2.id = 2;
		p2.t = TILE_O;

		boolean running = true;

		start();

		while (running) {
			draw();
			running = update();
		}

		draw();
		stop();
	}

	public static void start() {
		currentPlayer = p1;
		board = new ArrayList<String>();
		for (int i = 0; i < 9; i++) {
			board.add(TILE_EMPTY);
		}
	}

	public static void stop() {
		System.out.println("the winner is " + currentPlayer.text);
	}

	public static void draw() {
		System.out.println("+---+---+---+");
		for (int i = 0; i < 9; i += 3) {
			System.out.format("| %s | %s | %s |%n", board.get(i), board.get(i + 1), board.get(i + 2));
			System.out.println("+---+---+---+");
		}
	}

	public static boolean update() {
		Scanner reader = new Scanner(System.in);
		System.out.print("choose your spot " + currentPlayer.text + ": ");
		String input = reader.next();

		int x = input.charAt(0) - 49;
		int y = input.charAt(2) - 49;

		if (board.get(x + y * 3) == TILE_EMPTY) {
			board.set(x + y * 3, currentPlayer.t);
		}

		if (check()) {
			return false;
		}

		if (currentPlayer.id == p1.id) {
			currentPlayer = p2;
		} else {
			currentPlayer = p1;
		}

		return true;
	}

	public static boolean check() {
		int[][] checks = new int[][] {
			{0, 1, 2},
			{3, 4, 5},
			{6, 7, 8},

			{0, 3, 6},
			{1, 4, 7},
			{2, 5, 8},

			{0, 4, 8},
			{2, 4, 6},
		};

		for (int i = 0; i < checks.length; i++) {
			if (board.get(checks[i][0]) == currentPlayer.t &&
				board.get(checks[i][1]) == currentPlayer.t &&
				board.get(checks[i][2]) == currentPlayer.t) {
				return true;
			}
		}

		return false;
	}

	static class Player {
		public String text;
		public int id;
		public String t;
	}
}
