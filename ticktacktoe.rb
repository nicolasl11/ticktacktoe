$board = []

$currentPlayer = nil

TILE_EMPTY = " "
TILE_X = "x"
TILE_O = "o"

PLAYER_1 = {
	"text" => "player 1 (x)",
	"id" => 1,
	"t" => TILE_X
}

PLAYER_2 = {
	"text" => "player 2 (o)",
	"id" => 2,
	"t" => TILE_O
}

def start
	9.times do
		$board << TILE_EMPTY
	end

	$currentPlayer = PLAYER_1
end

def stop
	puts "the winner is #{$currentPlayer["text"]}"
end

def update
	print "choose your spot #{$currentPlayer["text"]} (hor,vert): "
	input = gets
	input = input.chomp
	x = input[0].to_i - 1
	y = input[2].to_i - 1

	if $board[x + y * 3] == TILE_EMPTY
		$board[x + y * 3] = $currentPlayer["t"]
	end

	if check()
		return false
	end

	if $currentPlayer == PLAYER_1
		$currentPlayer = PLAYER_2
	else
		$currentPlayer = PLAYER_1
	end

	return true
end

def draw
	puts "+---+---+---+"

	[0, 3, 6].each do |item|
		puts "| #{$board[item]} | #{$board[item + 1]} | #{$board[item + 2]} |"
		puts "+---+---+---+"
	end
end

def check
	checks = [
		[0, 1, 2],
		[3, 4, 5],
		[6, 7, 8],

		[0, 3, 6],
		[1, 4, 7],
		[2, 5, 8],
		
		[0, 4, 8],
		[2, 4, 6]
	]

	checks.each do |list|
		if $board[list[0]] == $currentPlayer["t"] and
			$board[list[1]] == $currentPlayer["t"] and
			$board[list[2]] == $currentPlayer["t"]
			return true
		end
	end

	return false
end

running = true

start()

while running do
	draw()
	running = update()
end

draw()
stop()
