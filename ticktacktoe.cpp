#include <iostream>
#include <string>
#include <vector>
#include <climits>
#include <cstdlib>

#define TILE std::string

const std::string TILE_EMPTY = " ";
const std::string TILE_X = "x";
const std::string TILE_O = "o";

struct Player {
	std::string text;
	int id;
	TILE t;
};

void start(std::vector<TILE>&, Player&, Player, Player);
void stop(Player);
void draw(std::vector<TILE>);
bool update(std::vector<TILE>&, Player&, Player, Player);
bool check(std::vector<TILE>&, Player&);

template <typename T>
T getInput(std::string text);

int main() {
	Player currentPlayer;
	Player p1, p2;
	std::vector<TILE> board;

	p1.text = "player 1 (x)";
	p1.id = 1;
	p1.t = TILE_X;

	p2.text = "player 2 (o)";
	p2.id = 2;
	p2.t = TILE_O;

	bool running = true;

	start(board, currentPlayer, p1, p2);

	while (running) {
		draw(board);
		running = update(board, currentPlayer, p1, p2);
	}

	draw(board);
	stop(currentPlayer);

	return 0;
}

void start(std::vector<TILE>& board, Player& currentPlayer, Player p1, Player p2) {
	currentPlayer = p1;
	for (int i = 0; i < 3 * 3; i++) {
		board.push_back(TILE_EMPTY);
	}
}

void draw(std::vector<TILE> board) {
	std::cout << "+---+---+---+" << std::endl;
	for (int i = 0; i < 3 * 3; i += 3) {
		std::cout << "| " << board.at(i) << " | " << board.at(i + 1) << " | " << board.at(i + 2) << " |" << std::endl;
		std::cout << "+---+---+---+" << std::endl;
	}
}

bool update(std::vector<TILE>& board, Player& currentPlayer, Player p1, Player p2) {
	std::string input = getInput<std::string>("choose your spot " + currentPlayer.text + ": ");
	int x = input.at(0) - 49;
	int y = input.at(2) - 49;

	if (board.at(x + y * 3) == TILE_EMPTY) {
		*&board.at(x + y * 3) = currentPlayer.t;
	}

	if (check(board, currentPlayer)) {
		return false;
	}

	if (currentPlayer.id == p1.id) {
		currentPlayer = p2;
	} else {
		currentPlayer = p1;
	}

	return true;
}

void stop(Player currentPlayer) {
	std::cout << "the winner is " << currentPlayer.text << std::endl;
}

bool check(std::vector<TILE>& board, Player& currentPlayer) {
	int checks[8][3] = {
		{0, 1, 2},
		{3, 4, 5},
		{6, 7, 8},

		{0, 3, 6},
		{1, 4, 7},
		{2, 5, 8},

		{0, 4, 8},
		{2, 4, 6}
	};

	for (int i = 0; i < 8; i++) {
		if (board.at(checks[i][0]) == currentPlayer.t &&
			board.at(checks[i][1]) == currentPlayer.t &&
			board.at(checks[i][2]) == currentPlayer.t) {
			return true;
		}
	}

	return false;
}

template <typename T>
T getInput(std::string text) {
	T token;
	std::cout << text;
	while (!(std::cin >> token)) {
		std::cout << "Foutive waarde, probeer opnieuw";
		std::cout << text;
		std::cin.clear();
		std::cin.ignore(INT_MAX, '\n');
	}
	return token;
}
