using System;
using System.IO;

public class Player {
	public string text {get;set;}
	public int id {get;set;}
	public string t {get;set;}
}

public class Tile {
	public static string EMPTY = " ";
	public static string X = "x";
	public static string O = "o";
}

public class Ticktacktoe {
	public static string[] board;
	public static Player currentPlayer, p1, p2;

	public static void Main(string[] args) {
		p1 = new Player();
		p1.text = "player 1 (x)";
		p1.id = 1;
		p1.t = Tile.X;

		p2 = new Player();
		p2.text = "player 2 (o)";
		p2.id = 2;
		p2.t = Tile.O;

		Start();

		bool running = true;

		while (running) {
			Draw();
			running = Update();
		}

		Draw();
		Stop();
	}

	public static void Start() {
		currentPlayer = p1;
		board = new string[3 * 3];
		for (int i = 0; i < 3 * 3; i++) {
			board[i] = Tile.EMPTY;
		}
	}

	public static void Stop() {
		Console.WriteLine("the winner is {0}", currentPlayer.text);
	}

	public static void Draw() {
		Console.WriteLine("+---+---+---+");
		for (int i = 0; i < 3 * 3; i += 3) {
			Console.WriteLine("| {0} | {1} | {2} |", board[i], board[i + 1], board[i + 2]);
			Console.WriteLine("+---+---+---+");
		}
	}

	public static bool Update() {
		Console.Write("choose your spot {0}: ", currentPlayer.text);
		string input = Console.ReadLine();

		int x = input[0] - 49;
		int y = input[2] - 49;

		Console.WriteLine(x + y * 3);

		if (board[x + y * 3] == Tile.EMPTY) {
			board[x + y * 3] = currentPlayer.t;
		}

		if (Check()) {
			return false;
		}

		if (currentPlayer.id == p1.id) {
			currentPlayer = p2;
		} else {
			currentPlayer = p1;
		}

		return true;
	}

	public static bool Check() {
		int[,] checks = new int[8,3] {
			{0, 1, 2},
				{3, 4, 5},
				{6, 7, 8},

				{0, 3, 6},
				{1, 4, 7},
				{2, 5, 8},

				{0, 4, 8},
				{2, 4, 6}
		};

		for (int i = 0; i < 8; i++) {
			if (board[checks[i, 0]] == Ticktacktoe.currentPlayer.t &&
					board[checks[i, 1]] == currentPlayer.t &&
					board[checks[i, 2]] == currentPlayer.t) {
				return true;
			}
		}

		return false;
	}
}
