#!/bin/bash

FILES=$(find . -maxdepth 1 -type f)

for file in $FILES; do
	echo -e "$file:\t$(wc -l $file | cut -d \  -f 1) lines,\t$(wc -c $file | cut -d \  -f 1) bytes" >> output.txt
done

if [ "$#" -eq 1 ]; then
	if [ "$1" = l ]; then
		echo "$(sort -k 2 -n output.txt)"
	elif [ "$1" = b ]; then
		echo "$(sort -k 4 -n output.txt)"
	else
		echo "argument not correct: correct one is eighter 'l' or 'b'"
	fi
fi

rm output.txt
